from django.db import models
from django.urls import reverse

# Create your models here.
class VinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    tech_name = models.CharField(max_length=200)
    tech_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.tech_name


class Appointment(models.Model):

    cx_name = models.CharField(max_length=200)
    date_time = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    service_type = models.CharField(max_length=200)
    vin = models.CharField(max_length=17, )
    status = models.BooleanField(default = False)

    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_list_appointments", kwargs={"pk": self.pk})

    def __str__(self):
        return self.cx_name

    class Meta:
        ordering = ("cx_name", "vin")
