from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Technician, Appointment, VinVO
import json


class VinVODetailEncoder(ModelEncoder):
    model = VinVO
    properties = [
        "import_href",
        "vin",
    ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "tech_name",
        "tech_id",
        "id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "cx_name",
        "vin",
        "date_time",
        "technician",
        "service_type",
        "status",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }

    def get_extra_data(self, o):
        count = VinVO.objects.filter(vin=o.vin).count()
        return {"vip": count > 0}


@require_http_methods(["GET", "POST"])
def api_list_appointments(request, vin_vo_id=None):
    if request.method == "GET":

        appointments = Appointment.objects.all()

        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            tech = content["technician"]
            technician = Technician.objects.get(tech_name=tech)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid Tech ID."}, status=400)

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointments(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"Delete": True})
    else:  # PUT
        appointment_complete = Appointment.objects.get(id=pk)
        appointment_complete.status = True
        appointment_complete.save()
        return JsonResponse(
            appointment_complete, encoder=AppointmentListEncoder, safe=False
        )

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create the technician"})
            response.status_code = 400
            return response
