import React, { useEffect, useState } from "react";


function AppointmentHistory() {

    const [appointments, setAppointments] = useState([]);
    const [search, setSearch] = useState('');


    const handleChange = async (e) => {
        setSearch(e.target.value);
    };

    const searchAppointments = async () => {
        const fetchUrl = `http://localhost:8080/api/appointments/`;
        const fetchResponse = await fetch(fetchUrl);

        if (fetchResponse.ok) {
            const fetchData = await fetchResponse.json();
            setAppointments(fetchData.appointments);
        }
    };


    useEffect(() => {
        searchAppointments();
    }, []);

    return (
        <>  <div className="gap-3 p-2 mt-5">
            <h2>Service History</h2>
            </div>
            <div className="p-4">
                <input
                    onChange={handleChange}
                    type="search"
                    placeholder="Search here"
                    className="form-control"
                    id="datatable-search-input">
                </input>
            </div>
            <table className="table table-striped shadow p-4 mt-2">
                <thead className="thead-dark">
                    <tr>
                        <th>Vin</th>
                        <th>Customer Name</th>
                        <th>Date & Time</th>
                        <th>Technician</th>
                        <th>Service Type</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter((appointment => {
                        return search === '' ? appointment : appointment.vin.includes(search)
                    })).map(appointment => {
                        const date = new Date(appointment.date_time)
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.cx_name}</td>
                                <td>{date.toLocaleString()}</td>
                                <td>{appointment.technician.tech_name}</td>
                                <td>{appointment.service_type}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}
export default AppointmentHistory;
