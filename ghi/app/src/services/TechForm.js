import React, { useState, useEffect } from 'react';

function TechForm() {

    const [techName, setTechName] = useState('');
    const handleTechNameChange = (event) => {
        const value = event.target.value;
        setTechName(value)
    }

    const [techID, setTechID] = useState('');
    const handleTechIDChange = (event) => {
        const value = event.target.value;
        setTechID(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.tech_name = techName;
        data.tech_id = techID;

        const techUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(techUrl, fetchConfig);
        if (response.ok) {
            const newTech = await response.json();

            setTechName('');
            setTechID('');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Technician</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleTechNameChange} value={techName} placeholder="tech name" required type="text" name="tech name"
                                id="tech name" className="form-control" />
                            <label htmlFor="name">Tech Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleTechIDChange} value={techID} placeholder="tech ID" required type="tech ID" name="tech ID"
                                id="tech ID" className="form-control" />
                            <label htmlFor="style">Tech ID</label>
                        </div>
                        <button className="btn btn-primary">Create Technician</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default TechForm;
