import React, { useEffect, useState } from "react";
import HistoryList from './HistoryList';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  const fetchAppointments = async () => {
    const fetchUrl = `http://localhost:8080/api/appointments/`
    const fetchResponse = await fetch(fetchUrl);

    if (fetchResponse.ok) {
      const fetchData = await fetchResponse.json();
      setAppointments(fetchData.appointments);
    }
  }
  useEffect(() => {
    fetchAppointments();
  }, []);

  const completeAppointment = async (id) => {
    const completeUrl = `http://localhost:8080/api/appointments/${id}/`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify({ "Completed": true }),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const completeResponse = await fetch(completeUrl, fetchConfig);
    if (completeResponse.ok) {
      toast("Great job, you crushed this appointment!");
      fetchAppointments();
    }
  };

  const cancelAppointment = async (id) => {
    const cancelUrl = `http://localhost:8080/api/appointments/${id}/`;
    const fetchConfig = {
      method: "DELETE",
      body: JSON.stringify(id),
      headers: {
        'Content-Type': 'application/json',
      }
    };
    const cancelResponse = await fetch(cancelUrl, fetchConfig);
    if (cancelResponse.ok) {
      toast("Bummer, maybe we'll help them next time!");
      fetchAppointments();
    }
  };

  const thumbsup = 'https://tinyurl.com/5n7h6ded'
  const thumbsdown = 'https://tinyurl.com/mr2nfce7'

  return (
    <>
      <div className="gap-3 p-2 mt-3">
        <h2>Upcoming Appointments:</h2>
      </div>
      <table className="table table-striped shadow p-4 mt-4">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Customer Name</th>
            <th>Date & Time</th>
            <th>Technician</th>
            <th>Service Type</th>
            <th>VIP</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments.filter((appointment) => {
            return appointment.status === false;
          }).map((appointment) => {
            const date = new Date(appointment.date_time)
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.cx_name}</td>
                <td>{date.toLocaleString()}</td>
                <td>{appointment.technician.tech_name}</td>
                <td>{appointment.service_type}</td>
                <td className="">{appointment.vip ? <img style={{ width: 30, height: 30 }} src={thumbsup}></img> : <img style={{ width: 30, height: 30 }} src={thumbsdown}></img>}</td>
                <td>
                  <button
                    type="button" className="btn btn-outline-success btn-xs" onClick={() => completeAppointment(appointment.id)}>Complete
                  </button>
                </td>
                <td>
                  <button
                    type="button" className="btn btn-danger btn-xs" onClick={() => cancelAppointment(appointment.id)}>Cancel
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <HistoryList />
      <ToastContainer
        position="bottom-right"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover={false}
        theme="dark" />
    </>
  );
}
export default AppointmentList;
