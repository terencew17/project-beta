import React, { useEffect, useState } from 'react';


function TechList() {

  const [technicians, setTechnician] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnician(data.technician)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <table className="table table-striped shadow mt-4">
      <thead className="thead-dark">
        <tr>
          <th>Technician Name</th>
          <th>Technician ID</th>
        </tr>
      </thead>
      <tbody className="align-items-centered">
        {technicians.map(technician => {
          return (
            <tr key={technician.id}>
              <td>{technician.tech_name}</td>
              <td>{technician.tech_id}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
export default TechList;
