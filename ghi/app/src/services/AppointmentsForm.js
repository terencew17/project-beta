import React, { useEffect, useState } from 'react';


function AppointmentsForm() {
    const [automobiles, setAutomobiles]= useState([])
    const [technician, setTechnician]= useState([])

    const [cx_name, setCxName] = useState('');
    const handleCxNameChange = (event) => {
        const value = event.target.value;
        setCxName(value)
    }

    const [vin, setVin] = useState('');
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value)
    }

    const [dateTime, setDateTime] = useState('');
    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value)
    }

    const [service_type, setServiceType] = useState('');
    const handleServiceTypeChange = (event) => {
        const value = event.target.value;
        setServiceType(value)
    }

    const [tech, setTech] = useState('');
    const handleTechChange = (event) => {
        const value = event.target.value;
        setTech(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.cx_name = cx_name;
        data.vin = vin;
        data.date_time = dateTime;
        data.service_type = service_type;
        data.technician = tech;

        const appointmentsUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(appointmentsUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();

            setCxName('');
            setVin('');
            setDateTime('');
            setServiceType('');
            setTech('');
            fetchAutomobilesData();
        }
    }
    const fetchAutomobilesData = async () => {
        const automobilesUrl = 'http://localhost:8100/api/automobiles/';
        const automobilesResponse = await fetch(automobilesUrl);

        if (automobilesResponse.ok) {
            const data = await automobilesResponse.json();
            setAutomobiles(data.autos)
        }
    }

    useEffect(() => {
        fetchAutomobilesData();
    }, []);


    const fetchTechnicianData = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const technicianResponse = await fetch(technicianUrl);

        if (technicianResponse.ok) {
            const data = await technicianResponse.json();
            setTechnician(data.technician)
        }
    }

    useEffect(() => {
        fetchTechnicianData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleCxNameChange} value={cx_name} placeholder="CxName" required type="text" name="Cxname"
                                id="Cxname" className="form-control" />
                            <label htmlFor="name">Customer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="vin"
                                id="vin" className="form-control" />
                            <label htmlFor="style">Vin</label>
                        </div>
                        <div>Date & Time</div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateTimeChange} value={dateTime} placeholder="YYYY-MM-DD HH:MM" required type="dateTime" name="dateTime" id="dateTime" className="form-control" />
                            <label htmlFor="dateTime">YYYY-MM-DD HH:MM</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleServiceTypeChange} value={service_type} placeholder="Service Type" type="text" name="serviceType" id="serviceType"
                                className="form-control" />
                            <label htmlFor="serviceType">Service Type</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleTechChange} value={tech} required id="tech" name="tech" className="form-select">
                                <option value="">Choose a Technician</option>
                                {technician.map(tech => {
                                    return (
                                        <option key={tech.id}>
                                            {tech.tech_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create Appointment</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default AppointmentsForm;
