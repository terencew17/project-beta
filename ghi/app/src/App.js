import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import TechForm from './services/TechForm'
import TechList from './services/TechList';
import AppointmentsForm from './services/AppointmentsForm';
import AppointmentsList from './services/AppointmentsList';
import HistoryList from './services/HistoryList';
import Nav from './Nav';
import CustomerForm from './sales/CreateCustomer';
import CreateSalesPerson from './sales/CreateSalesPerson';
import SalesForm from './sales/SalesForm';
import SalesPersonHistory from './sales/SalesPersonHistory';
import SalesRecordList from './sales/SalesList';
import ManufacturerList from './inventory/ManufacturerList';
import ManufacturerForm from './inventory/ManufacturerForm';
import VehicleModelForm from './inventory/VehicleModelForm';
import VehicleModelList from './inventory/VehicleModelList';
import InventoryForm from './inventory/InventoryForm';
import InventoryList from './inventory/InventoryList';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/customer" element={<CustomerForm />} />
          <Route path="/salesperson" element={<CreateSalesPerson />} />
          <Route path="inventory">
            <Route index element={<InventoryList />} />
            <Route path="create" element={<InventoryForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesRecordList />} />
            <Route path="create" element={<SalesForm />} />
            <Route path="history" element={<SalesPersonHistory />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>
          <Route path="vehicle">
            <Route index element={<VehicleModelList />} />
            <Route path="create" element={<VehicleModelForm />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechForm />} />
            <Route path="" element={<TechList technicians={props.technicians} />} />
          </Route>
          <Route path="appointments">
            <Route path="new" element={<AppointmentsForm />} />
            <Route path="" element={<AppointmentsList appointments={props.appointments} />} />
            <Route path="" element={<HistoryList appointments={props.appointments} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
