import React, { useEffect, useState } from 'react';

function InventoryList() {

    const[automobiles, setAutomobiles] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])
    return (
        <>
        <h2 className="gap-3 p-2 mt-3">Automobile Inventory</h2>
        <table className="table table-striped shadow p-4 mt-4">
        <thead>
          <tr>
            <th scope="col">VIN</th>
            <th scope="col">Color</th>
            <th scope="col">Year</th>
            <th scope="col">Model</th>
            <th scope="col">Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map(automobile => {
            return (
              <tr key={automobile.id} value={automobile.id}>
                <td>{ automobile.vin }</td>
                <td>{ automobile.color }</td>
                <td>{ automobile.year }</td>
                <td>{ automobile.model.name }</td>
                <td>{ automobile.model.manufacturer.name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    )
}

export default InventoryList
