import React, { useEffect, useState } from "react";

function VehicleModelList() {
  const [vehicles, setVehicles] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setVehicles(data.models);
    }
  }

  useEffect(() => {
    fetchData();
  }, [])

  return (
    <>
      <h2 className="gap-3 p-2 mt-3 ">Vehicle Models</h2>
      <div className="gap-4 row pt- mb-3">
        {vehicles.map(vehicle => {
          return (
            <div key={vehicle.id} value={vehicle.id} className="shadow p-4 mt-4 card mb-3 shadow square border border-danger w-25">
              <img src={vehicle.picture_url} className="card-img-top img-thumbnail img" />
              <div className="card-body"></div>
              <h5 className="card-title">{vehicle.name}</h5>
              <h5 className="card-title">{vehicle.manufacturer.name}</h5>
            </div>
          );
        })}
      </div>
    </>
  )
}
export default VehicleModelList
