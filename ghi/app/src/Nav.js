import { NavLink } from "react-router-dom";
import { Link } from "react-router-dom";

function Nav() {
  return (
    <nav
      className="navbar navbar-expand-lg navbar-dark"
      style={{ backgroundColor: "#FF4949" }}
    >
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          <img src="logo.jpeg" style={{ width: "75px" }} />
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <button
                className="btn dropdown-toggle"
                type="button"
                id="dropdown"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <img
                  src="https://tinyurl.com/3nxabjau"
                  className="pe-1 d-inline-block align-text-top"
                  style={{ width: "30px" }}
                />
                Inventory
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <li>
                  <Link className="dropdown-item" to="inventory">
                    Inventory
                  </Link>
                  <Link className="dropdown-item" to="inventory/create">
                    Add to Inventory
                  </Link>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <button
                className="btn dropdown-toggle"
                type="button"
                id="dropdown"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <img
                  src="https://img.icons8.com/ios/2x/automotive.png"
                  className="pe-1 d-inline-block align-text-top"
                  style={{ width: "30px" }}
                />
                Vehicles
              </button>
              <ul
                className="dropdown-menu me-auto"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <li>
                  <Link className="dropdown-item" to="manufacturers">
                    Manufacturers
                  </Link>
                  <Link className="dropdown-item" to="manufacturers/create">
                    Add Manufacturers
                  </Link>
                  <Link className="dropdown-item" to="vehicle">
                    Models
                  </Link>
                  <Link className="dropdown-item" to="vehicle/create">
                    Add Models
                  </Link>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <button
                className="btn dropdown-toggle"
                type="button"
                id="dropdown"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <img
                  src="https://tinyurl.com/3nxabjau"
                  className="pe-1 d-inline-block align-text-top"
                  style={{ width: "30px" }}
                />
                Inventory
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <li>
                  <Link className="dropdown-item" to="inventory">
                    Inventory
                  </Link>
                  <Link className="dropdown-item" to="inventory/create">
                    Add to Inventory
                  </Link>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <button
                className="btn dropdown-toggle"
                type="button"
                id="dropdown"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <img
                  src="https://img.icons8.com/ios/2x/cash-in-hand.png"
                  className="pe-1 d-inline-block align-text-top"
                  style={{ width: "30px" }}
                />
                Sales
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <li>
                  <Link className="dropdown-item" to="sales">
                    Sales Record
                  </Link>
                  <Link className="dropdown-item" to="sales/history">
                    Filter Sales Record
                  </Link>
                  <Link className="dropdown-item" to="sales/create">
                    Add New Sales
                  </Link>
                  <Link className="dropdown-item" to="salesperson">
                    Add Sales Person
                  </Link>
                  <Link className="dropdown-item" to="customer">
                    Add New Customer
                  </Link>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <button
                className="btn dropdown-toggle"
                type="button"
                id="dropdown"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <img
                  src="https://img.icons8.com/ios/2x/maintenance.png"
                  className="pe-1 d-inline-block align-text-top"
                  style={{ width: "30px" }}
                />
                Services
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <li>
                  <Link className="dropdown-item" to="/technicians">
                    Technicians
                  </Link>
                  <Link className="dropdown-item" to="/technicians/new">
                    Add New Technician
                  </Link>
                  <Link className="dropdown-item" to="/appointments">
                    Appointments
                  </Link>
                  <Link className="dropdown-item" to="/appointments/new">
                    Add New Appointment
                  </Link>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
