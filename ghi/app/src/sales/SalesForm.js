import React, { useEffect, useState } from 'react';

function SalesForm() {

    const[automobiles, setAutomobiles] = useState([]);
    const[customers, setCustomers] = useState([]);
    const[salesPersons, setSalesPersons] = useState([]);
    const[sales, setSales] = useState([]);

    const [automobile, setAutomobile] = useState('');
    const [salesPerson, setSalesPerson] = useState('');
    const [customer, setCustomer] = useState('');
    const [salePrice, setSalePrice] = useState('');

    const handleAutomobileChange = (event) => {
        setAutomobile(event.target.value);
    }
    const handleSalesPersonChange = (event) => {
        setSalesPerson(event.target.value);
    }
    const handleCustomerChange = (event) => {
        setCustomer(event.target.value);
    }
    const handlePriceChange = (event) => {
        setSalePrice(event.target.value);
    }

    const fetchSalesData = async() => {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const salesResponse = await fetch(salesUrl);
        if (salesResponse.ok) {
            const data = await salesResponse.json();
            setSales(data.sales)
        }
    }

    const fetchAutomobilesData = async() => {
        const automobilesUrl = 'http://localhost:8100/api/automobiles/';
        const automobilesResponse = await fetch(automobilesUrl);
        if (automobilesResponse.ok) {
            const data = await automobilesResponse.json();
            setAutomobiles(data.autos)
        }
    }

    const fetchSalesPersonData = async() => {
        const salesPersonUrl = 'http://localhost:8090/api/sales/employee/';
        const salesPersonsResponse = await fetch(salesPersonUrl);
        if (salesPersonsResponse.ok) {
            const data = await salesPersonsResponse.json();
            setSalesPersons(data.sales_person)
        }
    }

    const fetchCustomerData = async() => {
        const customerUrl = 'http://localhost:8090/api/sales/customer/';
        const customerResponse = await fetch(customerUrl);
        if (customerResponse.ok) {
            const data = await customerResponse.json();
            setCustomers(data.customers)
        }
    }

    useEffect(() => {
        fetchAutomobilesData();
        fetchSalesPersonData();
        fetchCustomerData();
        fetchSalesData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.automobile = automobile;
        data.customer = customer;
        data.sales_person = salesPerson;
        data.price = salePrice;

        const salesRecordUrl = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }

        const response = await fetch(salesRecordUrl, fetchConfig);
        if (response.ok) {
            const newSales = await response.json();
            console.log(newSales);

            setAutomobiles(auto => {
                return auto.filter(auto => auto.vin !== automobile)
            })

            setAutomobile('');
            setCustomer('');
            setSalesPerson('');
            setSalePrice('');
        }
    }

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a New Sales Record</h1>
              <form onSubmit={handleSubmit} id="create-sales-form">
                <div className="mb-3">
                    <select onChange={handleAutomobileChange} value={automobile} required id="automobile" name="automobile" className="form-select">
                        <option value="">Choose an Automobile</option>
                        {automobiles.filter((auto) => {
                            return !sales.some(sale => sale.automobile.vin === auto.vin)
                        }).map(auto => {
                            return(
                                <option key={auto.vin} value={auto.vin}>
                                    {auto.vin}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select onChange={handleSalesPersonChange} value={salesPerson} required id="sales_person" name="sales_person" className="form-select">
                        <option value="">Choose a Sales Person</option>
                        {salesPersons.map(salePerson => {
                        return (
                            <option key={salePerson.id} value={salePerson.id}>
                                {salePerson.name}
                            </option>
                        )
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select onChange={handleCustomerChange} value={customer} required id="customer" name="customer" className="form-select">
                        <option value="">Choose a Customer</option>
                        {customers.map(customer => {
                        return (
                            <option key={customer.id} value={customer.id}>
                                {customer.name}
                            </option>
                        )
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePriceChange} value={salePrice} placeholder="Price" type="number" name="price" id="price" className="form-control" />
                    <label forhtml="starts">Price</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    )
}
export default SalesForm
