import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function load() {
  const techResponse = await fetch('http://localhost:8080/api/technicians/');
  const appointmentsResponse = await fetch('http://localhost:8080/api/appointments/');
  if (techResponse.ok && appointmentsResponse.ok) {
    const techData = await techResponse.json();
    const appointmentsData = await appointmentsResponse.json();
    root.render(
      <React.StrictMode>
        <App
        technicians={techData.technician}
        appointments={appointmentsData.appointments}
         />
      </React.StrictMode>
    );
  } else {
    console.error(techResponse && appointmentsResponse);
  }
}
load();
