import "react-responsive-carousel/lib/styles/carousel.min.css";

function MainPage() {
  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-light mx-auto d-block w-75">
        <div className="row justify-content-center">
          <div className="row">
            <div className="col-lg-4 col-md-12 mb-4 mb-lg-0">
              <img
                src="https://tinyurl.com/3u74e5ev"
                className="w-100 shadow-1-strong rounded mb-4"
                alt="underhood"
              />

              <img
                src="https://tinyurl.com/2nn3dupv"
                className="w-100 shadow-1-strong rounded mb-4"
                alt="engine that could"
              />
            </div>

            <div className="col-lg-4 mb-4 mb-lg-0">
              <img
                src="https://tinyurl.com/yc262x4b"
                className="w-100 shadow-1-strong rounded mb-4"
                alt="blinker fluid"
              />

              <img
                src="https://tinyurl.com/53yrtsn8"
                className="w-100 shadow-1-strong rounded mb-4"
                alt="underneath"
              />
            </div>

            <div className="col-lg-4 mb-4 mb-lg-0">
              <img
                src="https://tinyurl.com/29sskbsr"
                className="w-100 shadow-1-strong rounded mb-4"
                alt="hard hat"
              />

              <img
                src="https://tinyurl.com/2wwu2c39"
                className="w-100 shadow-1-strong rounded mb-4"
                alt="exhaust"
              />
            </div>
          </div>
          <h1 className="mt-4 display-5 fw-bold">Apex Automotive</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              The premiere solution for automobile dealership management!
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default MainPage;
