from django.contrib import admin
from .models import PotentialCustomer, SalesPerson, SalesRecord

@admin.register(PotentialCustomer)
class PotentialCustomerAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesPerson)
class SalesPersonAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesRecord)
class SalesRecord(admin.ModelAdmin):
    pass
