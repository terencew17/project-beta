from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import AutomobileVO, PotentialCustomer, SalesPerson, SalesRecord

# ADD a sales person -DONE
# ADD a potential customer- DONE
# Create a sales record - DONE
# List ALL sales- DONE
# List a sales person's history

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "year",
        "vin",
        "color",
        "id",
    ]

class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = [
        "name",
        "address",
        "phone_number",
        "id",
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "id"
    ]

class SalesRecordsEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "automobile",
        "sales_person",
        "customer",
        "price",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "customer": PotentialCustomerEncoder(),
        "sales_person": SalesPersonEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = SalesRecord.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesRecordsEncoder
        )
    else: # POST METHOD
        content = json.loads(request.body)
        try:

            sales_person_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=sales_person_id)
            content["sales_person"] = sales_person

            customer_id = content["customer"]
            customer = PotentialCustomer.objects.get(id=customer_id)
            content["customer"] = customer

            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile

            sales = SalesRecord.objects.create(**content)

            return JsonResponse(
                {"sales": sales}, ########
                encoder = SalesRecordsEncoder,
                safe = False
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Error occurred when creating a sales record!"},
                status = 400
            )

@require_http_methods(["GET", "DELETE"])
def api_sale_details(request, pk):
    if request.method == "GET":
        sales = SalesRecord.objects.get(id = pk)
        return JsonResponse(
            {"sales": sales},
            encoder=SalesRecordsEncoder,
            safe=False
        )
    else: # DELETE METHOD
        request.method == "DELETE"
        count, _ = SalesRecord.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )

@require_http_methods(["GET", "POST"])
def api_sales_person_list(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder
        )
    else: # POST METHOD
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe = False
        )

@require_http_methods(["GET"])
def api_employee_sales_list(request, pk):
    if request.method == "GET":
        try:
            sales = SalesRecord.objects.filter(sales=pk)
            return JsonResponse(
                sales,
                encoder=SalesRecordsEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Could not find sales records"},
                status = 400
            )

@require_http_methods(["GET", "POST"])
def api_customer_list(request):
    if request.method == "GET":
        customer = PotentialCustomer.objects.all()
        return JsonResponse(
            {"customers": customer},
            encoder=PotentialCustomerEncoder
        )
    else: # POST METHOD
        content = json.loads(request.body)
        customer = PotentialCustomer.objects.create(**content)
        return JsonResponse (
            customer,
            encoder=PotentialCustomerEncoder,
            safe=False
        )

@require_http_methods(["GET", "DELETE"])
def api_customer_details(request,pk):
    if request.method == "GET":
        customer = PotentialCustomer.objects.filter(id=pk)
        return JsonResponse(
            customer,
            encoder=PotentialCustomerEncoder,
            safe=False
        )
    else: # DELETE METHOD
        request.method == "DELETE"
        count, _ = PotentialCustomer.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
